﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MySql.Data.MySqlClient;


namespace MysqlConnectTutorial
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
            DataSet ds = new DataSet();
            MySqlConnection conexao = new MySqlConnection("Server=localhost;database=localhost_fiesp;uid=root;pwd=root");

            try
            {
                //abre a conexao
                conexao.Open();
            }
            catch (System.Exception e)
            {
                MessageBox.Show(e.Message.ToString());
            }

            //verificva se a conexão esta aberta
            if (conexao.State == ConnectionState.Open)
            {
                MySqlCommand cmd = conexao.CreateCommand();
                cmd.CommandText = "SELECT nome as nome, email as email, mensagem as mensagem FROM localhost_fiesp.fiesp_fale_conosco limit 0,40";

                //cria um adapter usando a instrução SQL para acessar a tabela Clientes
                MySqlDataAdapter adapter = new MySqlDataAdapter(cmd);
                //preenche o dataset via adapter
                adapter.Fill(ds);
                dataGridView1.DataSource = ds.Tables[0].DefaultView;

                dataGridView1.AutoResizeColumns(DataGridViewAutoSizeColumnsMode.AllCellsExceptHeader);
            }
        }

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }
    }
}
